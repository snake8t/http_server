pub struct RequestBuf {
    pub buf: Vec<u8>,
    pub has_seq_end: bool
}

impl RequestBuf {
    pub fn new(buf : Vec<u8>, has_seq_end : bool) -> RequestBuf {
        RequestBuf { buf, has_seq_end }
    }
}