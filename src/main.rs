use std::io;
use std::net::TcpListener;
use std::net::TcpStream;
use std::io::Write;
use std::io::Read;
use std::time::{Duration};
use std::ops::Add;
use std::prelude::v1::Vec;

extern crate chrono;
use chrono::{DateTime, Utc};


mod server_addr;
mod request_buf;
mod request;

const READ_BUFFER_SIZE : usize = 128;
const SERVER : &str = "http_server";

use quicli::prelude::*;
use structopt::StructOpt;

/// Read some lines of a file
#[derive(Debug, StructOpt)]
struct Cli {
    /// Input file to read
    file: String,
    /// Number of lines to read
    #[structopt(short = "n")]
    num: usize,
}

fn main() {

//    let args = Cli::from_args();
//    read_file(&args.file)?
//        .lines()
//        .take(args.num)
//        .for_each(|line| println!("{}", line));

    let addr = server_addr::Addr { host : "127.0.0.1:".to_string(), port: "7878".to_string() };
    let listener = TcpListener::bind(addr.host + addr.port.as_str()).unwrap();
    listener.set_ttl(100).expect("Couldn't set TTL");
    listener.set_nonblocking(true).expect("Cannot set non-blocking");
    println!("Server started on port: {} ", addr.port);


    for stream in listener.incoming() {
        match stream {
            Ok(s) => {
                let now: DateTime<Utc> = Utc::now();
                println!("{} Connection established", now);
                s.set_read_timeout(Some(Duration::new(10, 0))).expect("");
                handle_connection(s);
            }
            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                //println!("Error while connecting");
                // wait until network socket is ready, typically implemented
                // via platform-specific APIs such as epoll or IOCP
                //wait_for_fd();
                continue;
            }
            Err(e) => panic!("encountered IO error: {}", e),
        }
    }

    println!("Server stopped");
}

fn handle_connection(mut stream : TcpStream){

    let request = read_stream(&mut stream);
    let raw_req = request.to_string();
    io::stdout().write_all(raw_req.as_bytes()).expect("Error while writing stdout");

    get_response(&request);

    write_stream(&mut stream);
}

fn get_response(request : &request::Request){

}

fn write_stream(mut stream : &TcpStream){
    let to_send = String::from("HTTP/1.1 200 OK\r\n\
        Connection: close\r\n\
        Server: ") + SERVER;
    stream.write_all(to_send.as_bytes()).expect("Error while writing");
    stream.flush().expect("error");
}

fn read_stream(mut stream : &TcpStream) -> request::Request{
    let mut is_end = false;
    let mut raw_request = String::new();
    let mut buf_str : String;
    while !is_end {
        let mut buf = [0;READ_BUFFER_SIZE];
        let read_result = stream.read(&mut buf);
        match read_result {
            Ok(_) =>{
                let response_buf = analyze_request_buf(buf);
                buf_str = String::from_utf8(response_buf.buf)
                    .expect("Error while converting");
                raw_request = raw_request.add(&mut buf_str);

                //println!("***********Received***********");
                //println!("*********** PART OF DATA ************");
                //println!("{}", buf_str);
                //println!("************ Amount of bytes ************");
                //println!("{}", c);

                if response_buf.has_seq_end {
                    is_end = true;
                }
            },
            Err(_) =>{
                println!("Error while reading stream!");
            }
        }
    }

    request::Request::new(raw_request)
}

fn analyze_request_buf(buf : [u8;READ_BUFFER_SIZE]) -> request_buf::RequestBuf {
    let mut last_symb_cr = true;
    let mut seq_crrf_count = 0;
    let mut to_read: Vec<u8> = Vec::new();
    let mut counter = 0;

    for i in 0..buf.len() {
        if char::from(buf[i]) == '\r' {
            last_symb_cr = true;
            //println!("Found CR");
        }
        else if char::from(buf[i]) == '\n' && last_symb_cr {
            //println!("Found LF");
            last_symb_cr = false;
            seq_crrf_count += 1;
        }
        else {
            //println!("usual symbol");
            last_symb_cr = false;
            seq_crrf_count = 0;
        }

        if seq_crrf_count == 2{
            return request_buf::RequestBuf::new(to_read, true);
        }

        to_read.insert(counter, buf[i]);
        counter += 1;
    }

    return request_buf::RequestBuf::new(to_read, false);
}