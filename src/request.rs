#![allow(dead_code)]

use std::collections::HashMap;

pub struct Request {
    pub headers : HashMap<String, String>,
    pub body : String,
    pub method : Method,
    pub method_str : String,
    pub request_uri : String,
    pub http_version : String,
    raw : String
}

impl Request {
    pub fn new(raw : String) -> Request{
        Request::initialize(raw)
    }

    fn initialize(raw : String) -> Request{
        let raw_bytes = raw.as_bytes();
        let mut pos = 0;
        let mut is_method_begin = false;
        let mut method_vec : Vec<u8> = Vec::new();
        for i in 0..raw_bytes.len(){
            let c = raw_bytes[i];

            match c {
                //Space
                32 =>{
                    println!("Space!");

                    if is_method_begin{
                        pos += 1;
                        break;
                    }

                    continue;
                },
                //Method symbols
                (65 ... 90) =>{
                    is_method_begin = true;
                    method_vec.push(c);
                },
                //Others
                _ =>{
                    panic!("Incorrect symbol!")
                }
            }

            pos += 1;
        }

        let method = String::from_utf8(method_vec).expect("Error while convert to String");
        println!("Method: {}", method);

        Request{
            method: Method::GET,
            request_uri: "".to_string(),
            http_version: "".to_string(),
            method_str : method,
            headers : HashMap::new(),
            body : String::new(),
            raw
        }
    }

    pub fn to_string(&self) -> String{
        return self.raw.clone();
    }
}

pub enum Method{
    OPTIONS,
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    TRACE,
    ExtensionMethod
}